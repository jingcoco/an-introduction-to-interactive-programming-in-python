# About the Course #

![rice-logo.jpg](https://bitbucket.org/repo/z6Egxy/images/2997804465-rice-logo.jpg)

This two-part course (part 2 is available here) is designed to help students with very little or no computing background learn the basics of building simple interactive applications. Our language of choice, Python, is an easy-to learn, high-level computer language that is used in many of the computational courses offered on Coursera. To make learning Python easy, we have developed a new browser-based programming environment that makes developing interactive applications in Python simple. These applications will involve windows whose contents are graphical and respond to buttons, the keyboard and the mouse.

The primary method for learning the course material will be to work through multiple "mini-projects" in Python. To make this class enjoyable, these projects will include building fun games such as Pong, Blackjack, and Asteroids. When you’ve finished our course, we can’t promise that you will be a professional programmer, but we think that you will learn a lot about programming in Python and have fun while you’re doing it.

Course Syllabus
Our course syllabus can be seen at www.codeskulptor.org/coursera/interactivepython.html.

[Link course](https://www.coursera.org/course/interactivepython1)

[Link codeskulptor](http://www.codeskulptor.org)

